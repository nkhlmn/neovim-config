## Installation instructions for macOS

### 1. Clone configuration
```git clone https://github.com/nikhilkamineni/neovim-config.git ~/.config/nvim```

### 2. Install minpac
```git clone https://github.com/k-takata/minpac.git ~/.config/nvim/pack/minpac/opt/minpac```

### 3. Install python neovim package
```$ pip3 install neovim```

### 4. Open neovim and install plugins
```:PackUpdate```
